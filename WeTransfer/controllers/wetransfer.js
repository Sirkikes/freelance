const { response } = require('express');
const { download } = require('wetransfert');

const wetransferGet = async (req, res = response) => {
    const { wtlink, folder } = req.query;
    download(wtlink, folder)
        .onProgress(progress => {
            console.log('progress', progress);
        })
        .then((resWt) => {
            console.log(resWt); // success
            res.send(`Se ha descargado correctamente el archivo.`);
        })
        .catch((err) => {
            console.error('error  ', err);
            res.send(`Ha ocurrido un error descargando el archivo.`);
        });
};

module.exports = {
    wetransferGet
};