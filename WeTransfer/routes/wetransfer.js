const { Router } = require('express');
const { wetransferGet } = require('../controllers/wetransfer');

const router = Router();

router.get('/', wetransferGet);

module.exports = router;