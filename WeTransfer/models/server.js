const express = require('express');
const cors = require('cors');

class Server {

    constructor() {
        this.app = express()
        this.port = process.env.PORT;

        //Midelwares
        this.midelwares();

        //Rutas
        this.routes();
    }

    midelwares() {
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.static('public'));
    }

    routes() {
        this.app.use('/api/wetransfer', require('../routes/wetransfer'));
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log(`Servidor corriendo por el puerto: ${this.port}`);
        });
    }

}

module.exports = Server;